from django.shortcuts import redirect

from django.urls import reverse_lazy
from django.views.generic.detail import DetailView

from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

# from django.contrib.auth.mixins import LoginRequiredMixin
# from django.views.decorators.http import require_http_methods
# from django.db import IntegrityError

from todos.models import TodoList, TodoItem


# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]

    def form_valid(self, form):
        plan = form.save(commit=True)
        return redirect("todo_list_detail", pk=plan.id)


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = "__all__"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.id])


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "items/new.html"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    fields = ["task", "due_date", "is_completed", "list"]
    template_name = "items/edit.html"

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])
